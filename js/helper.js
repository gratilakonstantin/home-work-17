const nameError = document.getElementById('name-error');
const passwordError = document.getElementById('password-error');
const ageError = document.getElementById('age-error');
const emailError = document.getElementById('email-error');
const phoneError = document.getElementById('phone-error');
const bankcardError = document.getElementById('bankCard-error');
const buttonError = document.getElementById('button-error');

function validateName(name) {
    if (name.match(/^[A-Za-z.\s_-]{1,20}$/)) {
        nameError.innerHTML = '';
        return true;
    } else {
        nameError.innerHTML = 'Enter the correct name';
        return false;
    }
}

function validatePassword(password) {
    if (password.match(/[0-9a-z]+/i)) {
        passwordError.innerHTML = '';
        return true;
    } else {
        passwordError.innerHTML = 'Enter the correct password';
        return false;
    }
}

function validateAge(age) {
    if (age.match(/^\d{1,3}$/)) {
        ageError.innerHTML = '';
        return true;
    } else {
        ageError.innerHTML = 'Enter the correct age';
        return false;
    }
}

function validateEmail(email) {
    if (email.match(/\w{1,12}@\w{2,}/)) {
        emailError.innerHTML = '';
        return true;
    } else {
        emailError.innerHTML = 'Enter the correct email';
        return false;
    }
}

function validatePhone(phone) {
    if (phone.match(/^\d{10}$/)) {
        phoneError.innerHTML = '';
        return true;
    } else {
        phoneError.innerHTML = 'Enter the correct phone';
        return false;
    }
}

function validateBankCard(bankcard) {
    if (bankcard.match(/^\d{16}$/)) {
        bankcardError.innerHTML = '';
        return true;
    } else {
        bankcardError.innerHTML = 'Enter the correct bankcard';
        return false;
    }
}

function validateForm(userObject) {
    if (
        validateName(userObject.name)
        && validatePassword(userObject.password)
        && validateAge(userObject.age)
        && validateEmail(userObject.email)
        && validatePhone(userObject.phone)
        && validateBankCard(userObject.bankCard)
    ) {
        return 'valid'
    } else {
        return 'invalid'
    }
}

